<?php
	function word2pdf($doc_url, $output_url){
		$word = new COM("Word.Application") or die ("Could not initialise Object.");
		// SET 0 TO DO NOT SEE MW WINDOW
		$word->Visible = 0;
		// DISABLE ALERTS
		$word->DisplayAlerts = 0;
		// OPEN DOC
		$word->Documents->Open($doc_url);
		// ACTUAL CONVERTING
		$word->ActiveDocument->ExportAsFixedFormat($output_url, 17, false, 0, 0, 0, 0, 7, true, true, 2, true, true, false);
		// QUIT WORD PROCESS
		$word->Quit(false);
		// CLEAN UP
		unset($word);
	}
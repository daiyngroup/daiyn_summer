<?php
	require("db.php");

	$data = array();
	$total_pages = 0;
	if($_POST["order_number"]){
		// CHECKING FOR EXISTENCE OF ORDER
		$sql = "SELECT total_pages FROM orders WHERE order_number=" . $_POST["order_number"];
		$result = $conn->query($sql);
		if($result->num_rows != 0){
			// GETTING TOTAL PAGES
			while($row = $result->fetch_assoc()){
	        	$total_pages += $row["total_pages"];
	    	}
			$data["total_pages"] = $total_pages;
			// GETTING DOCUMENT VALUES
			$data["docs"] = array();
			$sql = "SELECT hash, list_copies FROM documents WHERE order_number=" . $_POST["order_number"];
			$result = $conn->query($sql);
			if($result->num_rows > 0){
				while($row = $result->fetch_assoc()){
					$docs_row["list_copies"] = $row["list_copies"];
					$docs_row["hash"] = $row["hash"];
	        		array_push($data["docs"], $docs_row);
	    		}
			}
			echo json_encode($data);
			$conn->close();
		} else {
			echo 404;
		}
	} else{
		echo 404;
	}
<?php
	require("fpdf/fpdf.php");

	class PDF_Rotate extends FPDF{
		var $angle = 0;

		function Rotate($angle, $x=-1, $y=-1){
			if($x == -1) $x = $this->x;
			if($y == -1) $y = $this->y;
			if($this->angle != 0) $this->_out('Q');
			$this->angle = $angle;
			if($angle != 0){
				$angle *= M_PI / 180;
				$c = cos($angle);
				$s = sin($angle);
				$cx = $x * $this->k;
				$cy = ($this->h - $y) * $this->k;
				$this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm', $c, $s, -$s, $c, $cx, $cy, -$cx, -$cy));
			}
		}

		function _endpage(){
			if($this->angle != 0){
				$this->angle = 0;
				$this->_out('Q');
			}
			parent::_endpage();
		}
	}

	class PDF extends PDF_Rotate{
		function RotatedImage($file, $x, $y, $w, $h, $angle){
			// Image rotated around its upper-left corner
			$this->Rotate($angle, $x, $y);
			$this->Image($file, $x, $y, $w, $h);
			$this->Rotate(0);
		}
	}

	// CREATING PDF FROM PIC
	function create_pdf_from_pic($current_dir, $img_name, $padding, $orientation, $images_number, $width_rel, $height_rel, $upload_dir){
		// STANDARD VALUES
		$page_width = 210;
		$page_height = 297;
		$page_left_margin = 30;
		$page_top_margin = 20;
		$page_bottom_margin = 20;
		$page_right_margin = 15;
		$img_width = $page_width * $width_rel;
		$img_height = $page_height * $height_rel;

		$pdf = new PDF();
		$pdf->SetAutoPageBreak(false);

		// CREATING ONE PAGE OF PDF
		$abscissa = 0;
		$ordinate = 0;
		if($padding == 1){
			$abscissa = $page_left_margin;
			$ordinate = $page_top_margin;
		}
		if($orientation == "L"){
			$abscissa += $img_height;
		}
		$count = 0;
		$pdf->AddPage("P", "A4");

		if($padding == 1 && $orientation == "P"){
			$elements_per_row = floor(($page_width - $page_left_margin - $page_right_margin) / $img_width);
		} else if($padding == 0 && $orientation == "P"){
			$elements_per_row = floor($page_width / $img_width);
		} else if($padding == 1 && $orientation == "L"){
			$elements_per_row = floor(($page_width - $page_left_margin - $page_right_margin) / $img_height);
		} else if($padding == 0 && $orientation == "L"){
			$elements_per_row = floor($page_width / $img_height);
		}
		$rows = ceil($images_number / $elements_per_row);
		for($j = 0; $j < $rows; $j++){
			for($k = 0; $k < $elements_per_row; $k++){
				if($count != $images_number){
					if($orientation == "P"){
						// Image(file, x(current), y(current), width, height);
						$pdf->Image($current_dir . $img_name, $abscissa, $ordinate, $img_width, $img_height);
						$abscissa += $img_width;
					} else if($orientation == "L"){
						// RotatedImage($file, x(current), y(current), width, height $angle)
						$pdf->RotatedImage($current_dir . $img_name, $abscissa, $ordinate, $img_width, $img_height, -90);
						$abscissa += $img_height;
					}
					// SetXY(abscissa, ordinate);
					$pdf->SetXY($abscissa, $ordinate);
					// COUNTING DRAWN IMAGES
					$count++;
				}
			}
			// SETTING X TO NEW ROW
			if($padding == 1){
				$abscissa = $page_left_margin;
			} else {
				$abscissa = 0;
			}
			if($orientation == "L"){
				$abscissa += $img_height;
			}
			// SETTING Y TO NEW ROW
			if($orientation == "P"){
				$ordinate += ($j + 1) * $img_height;
			} else if($orientation == "L"){
				$ordinate += ($j + 1) * $img_width;
			}
		}

		list($hash, ) = explode(".", $img_name);

		$pdf->Output("F", $upload_dir . $hash . ".pdf");
		// unlink($current_dir . $img_name);
	}

	create_pdf_from_pic("C:/OpenServer/domains/abay/", "photo.jpg", 1, "L", 5, 0.27, 0.27, "C:/OpenServer/domains/abay/");
<?php
	require("create_hash.php");
	require("get_extension_from_name.php");
	require("get_pages_number_pdf.php");
	require("get_pages_number_word.php");
	require("dir.php");

	if($_FILES["file"]["tmp_name"]){
		global $dir, $domain_dir, $temp_dir;
		$hash = create_hash(16);
		$ext = get_extension_from_name($_FILES["file"]["name"]);
		$file_name = $hash . "." . $ext;
		move_uploaded_file($_FILES["file"]["tmp_name"], $temp_dir . $file_name);
		if($ext == "pdf"){
			echo get_pages_number_pdf($file_name);
			unlink($temp_dir . $file_name);
		} else if($ext == "doc" || $ext == "docx"){
			echo get_pages_number_word($file_name);
			unlink($temp_dir . $file_name);
		} else {
			echo "extension is not allowed";
		}
	} else {
		echo 404;
	}
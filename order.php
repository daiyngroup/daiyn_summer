<?php
	require("fpdf/fpdf.php");
	require("db.php");
	require("dir.php");
	require("word2pdf.php");
	require("create_pdf_from_pic.php");
	require("create_hash.php");
	require("create_order_number.php");
	require("get_extension_from_name.php");

	global $dir, $domain_dir, $temp_dir, $uploads_dir;

	$order_number;

	if($_POST["data"]){
		// GETTING DATA
		$data = (array)json_decode($_POST["data"]);
		// MAKING DIRECTORY
		$order_number = create_order_number(8);
		$dir = "uploads/" . $order_number . "/";
		$stmt = $conn->prepare("INSERT INTO orders (order_number, phone_number, order_time) VALUES (?, ?, ?)");
		$stmt->bind_param("iss", $order_number, $phone_number, $time);
		$phone_number = $_POST["phone_number"];
		if(mkdir($dir)){
			$time = date("Y-m-d H-i-s", time());
			$stmt->execute();
			$stmt->close();
		}
		// DEFAULT VALUES FOR STATEMENT
		$default_copies = 1;
		$default_side = 1;
		$default_sort = "sequence";
		// GETTING FILES ITSELF
		$counter = 0;
		foreach($data as $key => $val){
			// CREATING FUTURE NAME OF FILE
			$file_name = create_hash();
			// PREPARING QUERY TO DB
			$stmt = $conn->prepare("INSERT INTO documents (order_number, hash, copies, side, sort) VALUES (?, ?, ?, ?, ?)");
			if($val->type == "image"){
				// ADJUST PARAMS
				create_pdf_from_pic($dir, $val, $file_name);
				// WRITE TO DB
				$stmt->bind_param("isiis", $order_number, $file_name, $default_copies, $default_side, $default_sort);
			} else if($val->type == "pdf"){
				move_uploaded_file($_FILES["files"]["tmp_name"][$counter], $dir . $file_name . ".pdf");
				// WRITE TO DB
				$stmt->bind_param("isiis", $order_number, $file_name, $val->copies, $val->side, $val->sort);
			} else {
				// FIRSTLY DOWNLOADING FILE
				move_uploaded_file($_FILES["files"]["tmp_name"][$counter], $dir . $_FILES["files"]["name"][$counter]);
				// PASS FILE NAME AS PARAM
				$doc_url = "C:/inetpub/wwwroot/daiyn.com/" . $dir . $_FILES["files"]["name"][$counter];
				$output_url = "C:/inetpub/wwwroot/daiyn.com/" . $dir . $file_name . ".pdf";
				word2pdf($doc_url, $output_url);
				unlink($dir . $_FILES["files"]["name"][$counter]);
				// WRITE TO DB
				$stmt->bind_param("isiis", $order_number, $file_name, $val->copies, $val->side, $val->sort);
			}
			$stmt->execute();
			$stmt->close();
			$counter++;
		}
		echo $order_number;
		$conn->close();
	} else {
		echo 404;
	}
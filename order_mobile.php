<?php
	require("db.php");
	require("dir.php");
	require("word2pdf.php");
	require("create_pdf_from_pic.php");
	require("create_hash.php");
	require("create_order_number.php");
	require("get_extension_from_name.php");
	require("send_email.php");

	global $dir, $domain_dir, $temp_dir, $uploads_dir;

	// PREPARING QUERY TO DB (SAME FO ALL CASES)
	$stmt_doc = $conn->prepare("INSERT INTO documents (order_number, hash, list_copies) VALUES (?, ?, ?)");

	if($_POST["state"] == "-2"){
		// WHEN ONLY ONE FILE SENT
		// ONLY FOR FIRST FILE
		$order_number = create_order_number(8);
		// OTHER DATA OF FILE
		$hash = create_hash(16);
		$list_copies = $_POST["list_copies"];
		$email = $_POST["email"];
		$total_pages = $_POST["total_pages"];
		$order_time = date("Y-m-d H-i-s", time());
		$order_dir = $uploads_dir . $order_number . "/";
		// CREATING FOLDER WITH NAME OF ORDER NUMBER
		if(mkdir($order_dir)){
			// TYPE OF REQUEST
			if($_POST["type"] == "image"){
				$img_name = $hash . "." . get_extension_from_name($_FILES["file"]["name"]);
				move_uploaded_file($_FILES["file"]["tmp_name"], $temp_dir . $img_name);
				create_pdf_from_pic($temp_dir,
									$img_name,
									$_POST["padding"],
									$_POST["orientation"],
									$_POST["images_number"],
									$_POST["width"],
									$_POST["height"],
									$order_dir);
				// WRITE IMAGE TO DB
				$stmt_doc->bind_param("ssi", $order_number, $hash, $list_copies);
			} else if($_POST["type"] == "pdf"){
				move_uploaded_file($_FILES["file"]["tmp_name"], $order_dir . $hash . ".pdf");
				// WRITE PDF TO DB
				$stmt_doc->bind_param("ssi", $order_number, $hash, $list_copies);
			} else {
				// FIRSTLY DOWNLOADING FILE
				move_uploaded_file($_FILES["file"]["tmp_name"], $temp_dir . $_FILES["file"]["name"]);
				// PASS FILE NAME AS PARAM
				$doc_url = $temp_dir . $_FILES["file"]["name"];
				$output_url = $order_dir . $hash . ".pdf";
				word2pdf($doc_url, $output_url);
				unlink($doc_url);
				// WRITE DOC TO DB
				$stmt_doc->bind_param("ssi", $order_number, $hash, $list_copies);
			}
			$stmt_doc->execute();
			// CLOSING DB
			$stmt_doc->close();
		} else {
			echo "Fatal: directory is not created";
		}
		// WRITING ORDER NUMBER TO DB
		$stmt_order_number = $conn->prepare("INSERT INTO orders (order_number, email, total_pages, order_time) VALUES (?, ?, ?, ?)");
		$stmt_order_number->bind_param("ssis", $order_number, $email, $total_pages, $order_time);
		$stmt_order_number->execute();
		$stmt_order_number->close();
		// SENDING EMAIL
		send_email($email, $order_number);
		// SENDING ORDER NUMBER TO MOBILE APP
		echo $order_number;
	} else if($_POST["state"] == "-1"){
		// FIRST STEP OF CYCLE
		// SAVING ORDER NUMBER IN SESSION
		$order_number = create_order_number(8);
		$hash = create_hash(16);
		$list_copies = $_POST["list_copies"];
		$order_dir = $uploads_dir . $order_number . "/";
		// CREATING FOLDER WITH NAME OF ORDER NUMBER
		if(mkdir($order_dir)){
			// SAVING ORDER NUMBER
			file_put_contents($_POST["id_phone"] . ".txt", $order_number);
			// TYPE OF REQUEST
			if($_POST["type"] == "image"){
				$img_name = $hash . "." . get_extension_from_name($_FILES["file"]["name"]);
				move_uploaded_file($_FILES["file"]["tmp_name"], $temp_dir . $img_name);
				create_pdf_from_pic($temp_dir,
									$img_name,
									$_POST["padding"],
									$_POST["orientation"],
									$_POST["images_number"],
									$_POST["width"],
									$_POST["height"],
									$order_dir);
				// WRITE IMAGE TO DB
				$stmt_doc->bind_param("ssi", $order_number, $hash, $list_copies);
			} else if($_POST["type"] == "pdf"){
				move_uploaded_file($_FILES["file"]["tmp_name"], $order_dir . $hash . ".pdf");
				// WRITE PDF TO DB
				$stmt_doc->bind_param("ssi", $order_number, $hash, $list_copies);
			} else {
				// FIRSTLY DOWNLOADING FILE
				move_uploaded_file($_FILES["file"]["tmp_name"], $temp_dir . $_FILES["file"]["name"]);
				// PASS FILE NAME AS PARAM
				$doc_url = $temp_dir . $_FILES["file"]["name"];
				$output_url = $order_dir . $hash . ".pdf";
				word2pdf($doc_url, $output_url);
				unlink($doc_url);
				// WRITE DOC TO DB
				$stmt_doc->bind_param("ssi", $order_number, $hash, $list_copies);
			}
			$stmt_doc->execute();
			// CLOSING DB
			$stmt_doc->close();
		} else {
			echo "Fatal: directory is not created";
		}
	} else if($_POST["state"] == "0"){
		// MIDDLE STEPS OF CYCLE
		// GETTING ORDER NUMBER
		$order_number = file_get_contents($_POST["id_phone"] . ".txt");
		$hash = create_hash(16);
		$list_copies = $_POST["list_copies"];
		$order_dir = $uploads_dir . $order_number . "/";
		// TYPE OF REQUEST
		if($_POST["type"] == "image"){
			$img_name = $hash . "." . get_extension_from_name($_FILES["file"]["name"]);
			move_uploaded_file($_FILES["file"]["tmp_name"], $temp_dir . $img_name);
			create_pdf_from_pic($temp_dir,
								$img_name,
								$_POST["padding"],
								$_POST["orientation"],
								$_POST["images_number"],
								$_POST["width"],
								$_POST["height"],
								$order_dir);
			// WRITE IMAGE TO DB
			$stmt_doc->bind_param("ssi", $order_number, $hash, $list_copies);
		} else if($_POST["type"] == "pdf"){
			move_uploaded_file($_FILES["file"]["tmp_name"], $order_dir . $hash . ".pdf");
			// WRITE PDF TO DB
			$stmt_doc->bind_param("ssi", $order_number, $hash, $list_copies);
		} else{
			// FIRSTLY DOWNLOADING FILE
			move_uploaded_file($_FILES["file"]["tmp_name"], $temp_dir . $_FILES["file"]["name"]);
			// PASS FILE NAME AS PARAM
			$doc_url = $temp_dir . $_FILES["file"]["name"];
			$output_url = $order_dir . $hash . ".pdf";
			word2pdf($doc_url, $output_url);
			unlink($doc_url);
			// WRITE DOC TO DB
			$stmt_doc->bind_param("ssi", $order_number, $hash, $list_copies);
		}
		$stmt_doc->execute();
		// CLOSING DB
		$stmt_doc->close();
	} else if($_POST["state"] == "1"){
		// END OF CYCLE
		// GETTING ORDER NUMBER
		$order_number = file_get_contents($_POST["id_phone"] . ".txt");
		$hash = create_hash(16);
		$list_copies = $_POST["list_copies"];
		$email = $_POST["email"];
		$total_pages = $_POST["total_pages"];
		$order_dir = $uploads_dir . $order_number . "/";
		// TYPE OF REQUEST
		if($_POST["type"] == "image"){
			$img_name = $hash . "." . get_extension_from_name($_FILES["file"]["name"]);
			move_uploaded_file($_FILES["file"]["tmp_name"], $temp_dir . $img_name);
			create_pdf_from_pic($temp_dir,
								$img_name,
								$_POST["padding"],
								$_POST["orientation"],
								$_POST["images_number"],
								$_POST["width"],
								$_POST["height"],
								$order_dir);
			// WRITE IMAGE TO DB
			$stmt_doc->bind_param("ssi", $order_number, $hash, $list_copies);
		} else if($_POST["type"] == "pdf"){
			move_uploaded_file($_FILES["file"]["tmp_name"], $order_dir . $hash . ".pdf");
			// WRITE PDF TO DB
			$stmt_doc->bind_param("ssi", $order_number, $hash, $list_copies);
		} else{
			// FIRSTLY DOWNLOADING FILE
			move_uploaded_file($_FILES["file"]["tmp_name"], $temp_dir . $_FILES["file"]["name"]);
			// PASS FILE NAME AS PARAM
			$doc_url = $temp_dir . $_FILES["file"]["name"];
			$output_url = $order_dir . $hash . ".pdf";
			word2pdf($doc_url, $output_url);
			unlink($doc_url);
			// WRITE DOC TO DB
			$stmt_doc->bind_param("ssi", $order_number, $hash, $list_copies);
		}
		$stmt_doc->execute();
		// CLOSING DB
		$stmt_doc->close();
		// WRITING ORDER NUMBER TO DB
		$stmt_order_number = $conn->prepare("INSERT INTO orders (order_number, email, total_pages, order_time) VALUES (?, ?, ?, ?)");
		$stmt_order_number->bind_param("ssis", $order_number, $email, $total_pages, $time);
		$email = $_POST["email"];
		$total_pages = $_POST["total_pages"];
		$time = date("Y-m-d H-i-s", time());
		$stmt_order_number->execute();
		$stmt_order_number->close();
		// SENDING EMAIL
		send_email($email, $order_number);
		// SENDING ORDER NUMBER TO MOBILE APP
		echo $order_number;
		// RELEASE OF RESOURCES
		unlink($_POST["id_phone"] . ".txt");
	} else {
		echo 404;
	}
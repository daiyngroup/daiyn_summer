<?php
	function get_extension_from_name($file_name){
		$rev_file_name = "";
		$len = strlen($file_name);
		for($i = $len - 1; $i >= 0; $i--){
			if($file_name[$i] != '.'){
				$rev_file_name .= $file_name[$i];
			} else {
				break;
			}
		}
		return strrev($rev_file_name);
	}
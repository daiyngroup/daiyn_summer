<?php
	require("db.php");
	$stmt = $conn->prepare("INSERT INTO feedbacks (user_name, user_phone_number, user_email, user_message) VALUES (?, ?, ?, ?)");
	$stmt->bind_param("ssss", $user_name, $user_phone_number, $user_email, $user_message);

	$user_name = $_POST["user_name"];
	$user_phone_number = $_POST["user_phone_number"];
	$user_email = $_POST["user_email"];
	$user_message = $_POST["user_message"];

	$stmt->execute();
	$stmt->close();
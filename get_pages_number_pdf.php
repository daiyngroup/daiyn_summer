<?php
	require("dir.php");
	
	function get_pages_number_pdf($file_name){
		global $dir, $domain_dir, $temp_dir;
		$pdfinfo = $dir . "pdfinfo.exe";
		$file = $temp_dir . $file_name;
		exec($pdfinfo . " " . $file, $output);
		$pagecount = 0;
		foreach($output as $op){
		if(preg_match("/Pages:\s*(\d+)/i", $op, $matches) === 1){
				$pagecount = intval($matches[1]);
				break;
			}
		}
		return $pagecount;
	}
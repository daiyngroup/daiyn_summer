<?php
	function create_order_number($len){
		$numbers = "0123456789";
		$order_number = "";
		for($i = 0; $i < $len; $i++){
			$order_number .= $numbers[rand(0, strlen($numbers)-1)];
		}
		return $order_number;
	}
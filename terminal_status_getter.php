<?php
	require("db.php");

	if($_POST["json-string"]){
		$data = (array)json_decode($_POST["json-string"]);
		var_dump($data);
		$stmt = $conn->prepare("INSERT INTO statuses (terminal_id, page_counter, printer_on, status_time) VALUES (?, ?, ?, ?)");
		$stmt->bind_param("siis", $terminal_id, $page_counter, $printer_on, $status_time);
		$terminal_id = $data["terminal_id"];
		$page_counter = $data["page_counter"];
		$printer_on = $data["printer_on"];
		$status_time = date("Y-m-d H-i-s", time());
		$stmt->execute();
		$stmt->close();
	} else {
		echo 404;
	}
var createItem = function(file, id){
	var fullname = file.name; // FULL.NAME.PNG
	var filename = "";
	var type = "";
	var pieces = fullname.split('.');
	type = pieces[pieces.length - 1]; // READY TYPE
	for(var i = 0; i < pieces.length - 1; i++) filename += pieces[i] + ".";
	filename = filename.substring(0, filename.length - 1); // READY NAME

	// CREATING DOM EXTENSION
	var extension = document.createElement("div");
	extension.innerHTML = type;
	extension.className = "extension";

	// CREATING DOM NAME
	var name = document.createElement("div");
	name.innerHTML = filename;
	name.className = "name";

	// CREATING DOM CLOSE
	var close = document.createElement("div");
	close.innerHTML = "&#10006;";
	close.className = "close";

	close.onclick = function(){
		// DELETING ITEM FROM DOM
		var itemId = this.parentNode.id;
		var item = document.getElementById(itemId);
		itemsList.removeChild(item);

		// DELETING ITEM FROM filesData
		delete filesData[itemId];
		filesNumberRefresh();

		// ADJUSTING WIDTH
		var filesNumberValue = filesNumber(filesData);
		var itemsListWidth = 170 * filesNumberValue;
		itemsList.style.width = itemsListWidth + "px";

		// ADJUSTING MARGIN-LEFT
		if((filesNumberValue + 1) % 5 == 1 && state < 0 && (itemsListWidth - Math.abs(state) < 850)) state += 850;
		itemsList.style.marginLeft = state + "px";
		if(filesNumberValue == 0){
			showYourFiles(false);
			showPrintOptions(false);
			showTotal(false);
			showPayment(false);
		}

		// REFRESH TOTAL
		refreshTotal();
	}

	// CREATING FILE DOM
	var file = document.createElement("div");
	file.className = "file";
	file.appendChild(name);
	file.appendChild(extension);

	if(filesData[id].type == "image"){
		// SHOWING IMAGE PREVIEW
		var image = new Image();
		var reader = new FileReader();
		image.onload = function(){
			// GETTING REAL WIDTH AND HEIGHT OF IMAGE
			filesData[id].real_width = this.width;
			filesData[id].real_height = this.height;
			filesData[id].image_src = this.src;

			// INITIAL SETTINGS OF WIDTH AND HEIGHT
			filesData[id].width = 165; // 210 - 30 - 15
			filesData[id].height = filesData[id].real_height * (filesData[id].width / filesData[id].real_width);
		}
		reader.onloadend = function(){
			image.src = reader.result;
		}
		reader.readAsDataURL(filesData[id].file);
	}

	file.onclick = function(){
		// ADJUSTING CURRENT FILE
		if(activeElementId != ""){
			// MAKING PREVIOUS FILE INACTIVE
			document.getElementById(activeElementId).childNodes[1].className = "file";
		}

		// STORING ID OF CURRENT ACTIVE FILE
		var itemId = this.parentNode.id;
		activeElementId = itemId;
		this.className = "file file-active";
		if(filesData[itemId].type == "image"){
			// SHOWING IMAGE BLOCK
			showImageBlock(true);
			showDocumentBlock(false);

			// ADJUSTING IMAGE LIST COPIES
			document.getElementById("image-list-copies").value = filesData[activeElementId].list_copies;

			// ADJUSTING PADDING
			if(filesData[activeElementId].padding == 1){
				showPaddingButton.click();
			} else {
				hidePaddingButton.click();
			}

			// ADJUSTING IMAGE NUMBER
			document.getElementById("images-number").value = filesData[activeElementId].images_number;

			// ADJUSTING ORIENTATION
			if(filesData[activeElementId].orientation == "P"){
				imagePortraitButton.click();
			} else {
				imageLandscapeButton.click();
			}

			// ADJUSTING IMAGE BLOCK
			document.getElementById("image-size-range").value = 100 - filesData[activeElementId].rel;

			// RENDER PAGE
			renderPage(filesData[activeElementId]);
		} else {
			// SHOWING DOCUMENT BLOCK
			showImageBlock(false);
			showDocumentBlock(true);

			// ADJUSTING LIST COPIES
			document.getElementById("document-list-copies").value = filesData[activeElementId].list_copies;

			// ADJUSTING PAGES
			// if(filesData[itemId].pages == 0){
				// allPages.click();
			// } else {
				// severalPages.click();
			// }			
		}

		// ADJUSTING NAME OF FILE
		document.getElementById("file-name").innerHTML = filesData[itemId].file.name;
	}

	file.onmouseover = function(){
		if(activeElementId != this.parentNode.id){
			this.className = "file file-active";
		}
	}

	file.onmouseleave = function(){
		if(activeElementId != this.parentNode.id){
			this.className = "file";
		}
	}
	
	var item = document.createElement("li");
	item.className = "item";
	item.id = id;
	item.appendChild(close);
	item.appendChild(file);
	return item;
}
var renderPage = function(item){
	// MAKING PAGE CLEAN
	clearPage();

	// ADDING EXACT NUMBER OF IMAGES
	for(var i = 0; i < item.images_number; i++){
		// CREATING IMAGE TO INSERT
		var image = new Image();
		image.src = item.image_src;

		// ADJUSTING CLASS ITSELF
		jss.set('.' + item.class, { "width": item.width * 1.5 + "px", "height": item.height * 1.5 + "px" });

		// ADJUSTING CLASS NAMES
		image.className = "sample-image " + item.class;

		// ACTUAL INSERTING
		imagePage.appendChild(image);
	}
}
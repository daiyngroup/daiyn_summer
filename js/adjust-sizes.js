var adjustSizes = function(item){
	if(item.orientation == "P" && item.padding == 1){
		item.standard_width = realPageWidth - leftPadding - rightPadding;
	} else if(item.orientation == "P" && item.padding == 0){
		item.standard_width = realPageWidth;
	} else if(item.orientation == "L" && item.padding == 1){
		item.standard_width = realPageHeight - leftPadding - rightPadding;
	} else if(item.orientation == "L" && item.padding == 0){
		item.standard_width = realPageHeight;
	}
	item.standard_height = item.real_height * (item.standard_width / item.real_width);
	// COPYING VALUES
	item.width = item.standard_width;
	item.height = item.standard_height;
}
var dropzone = document.getElementById("dropzone");

dropzone.ondrop = function(e){
	e.preventDefault();
	this.className = "dropzone";
	// ALLOWING ONLY APPROPRIATE FILES AND ADDING THEM TO ARRAY
	addFilteredFiles(e.dataTransfer.files);
	// SHOWING FILES IN ITEMS LIST
	showPreparedFiles(filesData);
	// SHOW YOUR FILES
	showYourFiles(true);
	// CLICKING LAST ELEMENT
	itemsList.childNodes[itemsList.childElementCount - 1].childNodes[1].click();
	// SHOW PRINT OPTIONS
	showPrintOptions(true);
	// REFRESH PAGES NUMBER
	refreshTotal();
}

dropzone.ondragover = function(e){
	this.className = "dropzone dragover";
	return false;
}

dropzone.ondragleave = function(e){
	this.className = "dropzone";
	return false;
}
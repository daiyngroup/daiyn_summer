var range = document.getElementById("image-size-range");

range.oninput = function(){
	filesData[activeElementId].rel = 100 - this.value;
	
	// CALCULATING WIDTH FOR COUNTING
	var calc_width;
	if(filesData[activeElementId].orientation == "P" && filesData[activeElementId].padding == 1){
		calc_width = 165;
	} else if(filesData[activeElementId].orientation == "P" && filesData[activeElementId].padding == 0){
		calc_width = 210;
	} else if(filesData[activeElementId].orientation == "L" && filesData[activeElementId].padding == 1){
		calc_width = 252;
	} else if(filesData[activeElementId].orientation == "L" && filesData[activeElementId].padding == 0){
		calc_width = 297;
	}

	// CALCULATING ACTUAL SIZES
	filesData[activeElementId].width = calc_width * filesData[activeElementId].rel;
	filesData[activeElementId].height = filesData[activeElementId].real_height * (filesData[activeElementId].width / filesData[activeElementId].real_width);

	// ADJUSTING CLASS ITSELF
	jss.set('.' + filesData[activeElementId].class, { "width": filesData[activeElementId].width * 1.5 + "px", "height": filesData[activeElementId].height * 1.5 + "px" });
}
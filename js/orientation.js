var imagePortraitButton = document.getElementById("image-portrait-btn");
var imageLandscapeButton = document.getElementById("image-landscape-btn");

imagePortraitButton.onclick = function(){
	filesData[activeElementId].orientation = "P";
	this.className = "button button-active";
	imageLandscapeButton.className = "button";
	// PAGE SETTINGS
	if(filesData[activeElementId].padding == 1){
		imagePage.className = "portrait-with-padding";
	} else {
		imagePage.className = "portrait-without-padding";
	}
}

imageLandscapeButton.onclick = function(){
	filesData[activeElementId].orientation = "L";
	this.className = "button button-active";
	imagePortraitButton.className = "button";
	// PAGE SETTINGS
	if(filesData[activeElementId].padding == 1){
		imagePage.className = "landscape-with-padding";
	} else {
		imagePage.className = "landscape-without-padding";
	}
}
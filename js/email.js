var emailItself = document.getElementById("email-itself");
var correctEmailItself = false;

emailItself.oninput = function(){
	// PHONE NUMBER VALIDATION
	correctEmailItself = isEmailCorrect(emailItself.value);

	if(correctEmailItself){
		showPrint(true);
	} else {
		showSnackbar("Введите email правильно");
	}
}

var isEmailCorrect = function(email){
	if(email.match(/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/) != null){
		return true;
	} else {
		return false;
	}
}
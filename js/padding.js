var showPaddingButton = document.getElementById("show-padding-btn");
var hidePaddingButton = document.getElementById("hide-padding-btn");

showPaddingButton.onclick = function(){
	filesData[activeElementId].padding = 1;
	this.className = "button button-active";
	hidePaddingButton.className = "button";
	// PAGE SETTINGS
	if(filesData[activeElementId].orientation == "P"){
		imagePage.className = "portrait-with-padding";
	} else {
		imagePage.className = "landscape-with-padding";
	}
}

hidePaddingButton.onclick = function(){
	filesData[activeElementId].padding = 0;
	this.className = "button button-active";
	showPaddingButton.className = "button";
	// PAGE SETTINGS
	if(filesData[activeElementId].orientation == "P"){
		imagePage.className = "portrait-without-padding";
	} else {
		imagePage.className = "landscape-without-padding";
	}
}
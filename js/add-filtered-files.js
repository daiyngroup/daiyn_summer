var addFilteredFiles = function(files){
	for(var i = 0; i < files.length; i++){
		if(appropriateFile(files[i])){
			var hash = createHash(16);
			filesData[hash] = new Object();
			// ADJUSTING FILE
			filesData[hash].file = files[i];
			if(files[i].type.includes("image")){
				// ADJUSTING TYPE
				filesData[hash].type = "image";
				// ADJUSTING COPIES
				filesData[hash].list_copies = 1;
				// ADJUSTING PADDING
				filesData[hash].padding = 1;
				// ADJUSTING IMAGE COPIES
				filesData[hash].images_number = 1;
				// ADJUSTING ORIENTATION
				filesData[hash].orientation = "P";
				// ADJUSTING RELATION
				filesData[hash].rel = 100;
				// REAL WIDTH
				filesData[hash].real_width = 0;
				// REAL HEIGHT
				filesData[hash].real_height = 0;
				// COUNTING WIDTH
				filesData[hash].width = 0;
				// COUNTING HEIGHT
				filesData[hash].height = 0;
				// ADJUSTING CLASS NAME
				filesData[hash].class = hash;
			} else {
				if(files[i].type.includes("pdf")){
					filesData[hash].type = "pdf";
				} else {
					filesData[hash].type = "doc";
				}
				// ADJUSTING COPIES
				filesData[hash].list_copies = 1;
				// ADJUSTING PAGES
				filesData[hash].pages = 0;
			}
		} else {
			alert(files[i].name + " is not allowable");
		}
	}
}
var visa = document.getElementById("visa");
var onay = document.getElementById("onay");
var visaNumber = document.getElementById("visa-number");
var onayNumber = document.getElementById("onay-number");
var visaNumberLabel = document.getElementById("visa-number-lbl");
var onayNumberLabel = document.getElementById("onay-number-lbl");
var balanceEnough = false;

visa.onclick = function(){
	visa.className = "button button-active";
	onay.className = "button";

	visaNumber.style.display = "inline";
	onayNumber.style.display = "none";

	visaNumberLabel.style.display = "inline";
	onayNumberLabel.style.display = "none";
}

onay.onclick = function(){
	visa.className = "button";
	onay.className = "button button-active";

	visaNumber.style.display = "none";
	onayNumber.style.display = "inline";

	visaNumberLabel.style.display = "none";
	onayNumberLabel.style.display = "inline";
}

visaNumber.oninput = function(){
	if(this.value.length == 16){
		// ADJUSTING LBL
		visaNumberLabel.innerHTML = "";

		// BUILDING FORM
		var form = new FormData();
		form.append('number', this.value);
		form.append('sum', price);

		// BUILDING XHR
		var xhr = new XMLHttpRequest();

		// WHEN XHR IS GOT
		xhr.onload = function(){
			if(parseInt(this.responseText) == 1){
				isBalanceEnough(true);
				showPhone(true);
			} else {
				isBalanceEnough(false);
				showPhone(false);
			}
		}

		// SENDING XHR
		xhr.open("POST", "balance_checker.php");
		xhr.send(form);
	} else {
		visaNumberLabel.innerHTML = "введите цифры карты правильно";
	}
}

onayNumber.oninput = function(){
	if(this.value.length == 16){
		// ADJUSTING LBL
		onayNumberLabel.innerHTML = "";

		// BUILDING FORM
		var form = new FormData();
		form.append('number', this.value);
		form.append('sum', price);

		// BUILDING XHR
		var xhr = new XMLHttpRequest();

		// WHEN XHR IS GOT
		xhr.onload = function(){
			if(parseInt(this.responseText) == 1){
				isBalanceEnough(true);
				showEmail(true);
			} else {
				isBalanceEnough(false);
				showEmail(false);
			}
		}

		// SENDING XHR
		xhr.open("POST", "balance_checker.php");
		xhr.send(form);
	} else {
		onayNumberLabel.innerHTML = "введите цифры карты правильно";
	}
}

var isBalanceEnough = function(enough){
	balanceEnough = enough;
	if(enough) {
		document.getElementById("balance").innerHTML = "средств на карте достаточно";
	} else {
		document.getElementById("balance").innerHTML = "средств на карте НЕдостаточно";
	}
}
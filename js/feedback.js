var feedbackButton = document.getElementById("feedback-btn");
var feedbackForm = document.getElementById("feedback-form");
var feedbackOverlay = document.getElementById("feedback-overlay");

feedbackButton.onclick = function(){
	showFeedback(true);
}

feedbackOverlay.onclick = function(){
	showFeedback(false);
}

var isPhoneNumberCorrect = function(number){
	if(number.length == 12){
		return number.match(/\+77\d{9}/) != null;
	} else {
		return false;
	}
}

var isEmailCorrect = function(email){
	return email.match(/^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i) != null;
}
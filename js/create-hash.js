var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";

var createHash = function(stringLength){
	var hash = "";
	for(var i = 0; i < stringLength; i++){
		var rnum = Math.floor(Math.random() * chars.length);
		hash += chars.substring(rnum, rnum + 1);
	}
	return hash;
}
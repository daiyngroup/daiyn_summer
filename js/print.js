var print = document.getElementById("print-button");

print.onclick = function(){
	var form = new FormData();
	// APPENDING FILES AND DATA
	for(var i in filesData){
		form.append('files[]', filesData[i].file);
	}
	form.append('data', JSON.stringify(filesData));
	form.append('email', emailItself.value);

	var xhr = new XMLHttpRequest();

	xhr.onload = function(){
		if(this.responseText){
			// WRITING ORDER NUBMER TO SESSION
			if(typeof(Storage) !== "undefined"){
				// IF WINDOW LOCAL STORAGE IS SUPPORTED
				localStorage.setItem("order_number", this.responseText);
			} else {
				alert("Номер Вашего заказа " + this.responseText);
			}
			// GOING TO SHOW ORDER NUMBER
			location.href = "http://daiyn.com/show_order.html";		
		}
	}

	xhr.open("POST", "../order_fake.php");
	xhr.send(form);
	document.getElementById("overlay").style.display = "block";
}
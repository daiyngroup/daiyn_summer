var appropriateFile = function(file){
	switch(file.type){
		case "image/jpeg":
			return true;
		case "image/jpg":
			return true;
		case "image/png":
			return true;
		case "application/pdf":
			return true;
		case "application/msword":
			return true;
		case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
			return true;
		default:
			return false;
	}
}
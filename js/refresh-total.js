var price = 0;
var pricePerPage = 20;

var pageCount = function(files){
	var counter = 0;
	for(var i in files){
		counter += files[i].list_copies;
	}
	return counter;
}

var refreshTotal = function(){
	var numberOfPages = pageCount(filesData);
	document.getElementById("total-pages-number").innerHTML = numberOfPages;
	price = numberOfPages * pricePerPage;
	document.getElementById("price").innerHTML = price;
}
/******************************************* WHAT TO PRINT *******************************************/
var showWhatToPrint = function(showed){
	if(showed){
		document.getElementById("what-to-print").style.display = "block";
	} else {
		document.getElementById("what-to-print").style.display = "none";
	}
}

/******************************************* SHOW YOUR FILES *******************************************/
var showYourFiles = function(showed){
	if(showed){
		document.getElementById("your-files").style.display = "block";
	} else {
		document.getElementById("your-files").style.display = "none";
	}
}

/******************************************* PRINT OPTIONS *******************************************/
var showPrintOptions = function(showed){
	if(showed){
		document.getElementById("print-options").style.display = "block";
	} else {
		document.getElementById("print-options").style.display = "none";
	}
}

/******************************************* TOTAL *******************************************/
var showTotal = function(showed){
	if(showed){
		document.getElementById("total").style.display = "block";
		refreshTotal();
	} else {
		document.getElementById("total").style.display = "none";
	}
}

/******************************************* PAYMENT *******************************************/
var showPayment = function(showed){
	if(showed){
		document.getElementById("payment").style.display = "block";
	} else {
		document.getElementById("payment").style.display = "none";
	}
}

/******************************************* EMAIL *******************************************/
var showEmail = function(showed){
	if(showed){
		document.getElementById("email").style.display = "block";
	} else {
		document.getElementById("email").style.display = "none";
	}
}

/******************************************* PRINT *******************************************/
var showPrint = function(showed){
	if(showed){
		document.getElementById("print").style.display = "block";
	} else {
		document.getElementById("print").style.display = "none";
	}
}

/******************************************* IMAGE BLOCK *******************************************/
var showImageBlock = function(showed){
	if(showed){
		document.getElementById("image-block").style.display = "block";
	} else {
		document.getElementById("image-block").style.display = "none";
	}
}

/******************************************* DOCUMENT BLOCK *******************************************/
var showDocumentBlock = function(showed){
	if(showed){
		document.getElementById("document-block").style.display = "block";
	} else {
		document.getElementById("document-block").style.display = "none";
	}
}

/******************************************* SHOW PREPARED FILES *******************************************/
var showPreparedFiles = function(files){
	// REFRESH FILES NUMBER LABEL
	filesNumberRefresh();

	// ADJUSTING WIDTH AND MARGIN-LEFT OF ITEMSLIST
	var filesNumberValue = filesNumber(files);
	state = -850 * Math.ceil(filesNumberValue / 5 - 1);
	itemsList.style.marginLeft = state + "px";
	itemsList.style.width = 170 * filesNumberValue + "px";

	// REMOVING ALL CHILD NODES
	while(itemsList.firstChild){
		itemsList.removeChild(itemsList.firstChild);
	}

	// ADDING ITEMS
	for(var i in files){
		var item = createItem(files[i].file, i);
		itemsList.appendChild(item);
	}

	// SHOW TOTAL
	showTotal(true);
	
	// SHOW PAYMENT
	showPayment(true);
}

/******************************************* SHOW FEEDBACK *******************************************/
var showFeedback = function(showed){
	if(showed){
		document.getElementById("feedback-form").style.display = "block";
		document.getElementById("feedback-overlay").style.display = "block";
	} else {
		document.getElementById("feedback-form").style.display = "none";
		document.getElementById("feedback-overlay").style.display = "none";
	}
}

/******************************************* SHOW SNACKBAR *******************************************/
var showSnackbar = function(message){
	var snackbar = document.getElementById("snackbar");
	snackbar.innerHTML = message;
	snackbar.className = "show";
	setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
}
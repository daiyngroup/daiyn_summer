var feedbackSendButton = document.getElementById("feedback-send-btn");
var feedbackForm = new FormData();

feedbackSendButton.onclick = function(){
	// GETTING DATA FROM FORM
	var userName = document.getElementById("user-name").value;
	var userPhoneNumber = document.getElementById("user-phone-number").value;
	var userEmail = document.getElementById("user-email").value;
	var userMessage = document.getElementById("user-message").value;
	if(!isPhoneNumberCorrect(userPhoneNumber)){
		showSnackbar("Введите номер телефона правильно");
	} else if(!isEmailCorrect(userEmail)){
		showSnackbar("Введите email правильно");
	} else {
		feedbackForm.append("user_name", userName);
		feedbackForm.append("user_phone_number", userPhoneNumber);
		feedbackForm.append("user_email", userEmail);
		feedbackForm.append("user_message", userMessage);
		// REINITIALIZING FORM
		document.getElementById("user-name").value = "";
		document.getElementById("user-phone-number").value = "";
		document.getElementById("user-email").value = "";
		document.getElementById("user-message").value = "";
		// PREPARING TO SEND
		var feedbackXhr = new XMLHttpRequest();
		feedbackXhr.onload = function(){
			showSnackbar("Спасибо за предложение!");
		}
		// SENDING
		feedbackXhr.open("POST", "../feedback.php");
		feedbackXhr.send(feedbackForm);
		showFeedback(false);
	}
}
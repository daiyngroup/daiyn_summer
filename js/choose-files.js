var chooseFile = document.getElementById("choose_file");

chooseFile.onchange = function(){
	// ALLOWING ONLY APPROPRIATE FILES AND ADDING THEM TO ARRAY
	addFilteredFiles(this.files);
	// SHOWING FILES IN ITEMS LIST
	showPreparedFiles(filesData);
	// SHOW YOUR FILES
	showYourFiles(true);
	// CLICKING LAST ELEMENT
	itemsList.childNodes[itemsList.childElementCount - 1].childNodes[1].click();
	// SHOW PRINT OPTIONS
	showPrintOptions(true);
	// REFRESH PAGES NUMBER
	refreshTotal();
}